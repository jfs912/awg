FROM python:3.11-slim

WORKDIR /app

RUN apt-get update && apt-get install -y \
    libgl1-mesa-glx \
    libxext6 \
    libx11-6 \
    libfontconfig1 \
    libfreetype6 \
    libxrender1 \
    libxkbcommon0 \
    libglib2.0-0 \
    libdbus-1-3 \
    libegl1-mesa \
    libxcb-icccm4 \
    libxcb-image0 \
    libxcb-keysyms1 \
    libxcb-randr0 \
    libxcb-render-util0 \
    libxcb-xinerama0 \
    libxcb-xinput0 \
    libxcb-xkb1 \
    libxi6 \
    libsm6 \
    libice6 && \
    apt-get clean && \
    rm -rf /var/lib/apt/lists/*

COPY . /app

RUN pip install -r requirements.txt

CMD ["python3", "main.py"]

